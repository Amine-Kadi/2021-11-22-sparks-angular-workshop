== Top Collègues #1

== Initialisation du projet

* Créer un nouveau projet :

[source]
----
ng new top-collegues --strict --routing false --style css
----

== (Optionnel) Bootstrap

* Installer Bootstrap :

[source]
----
npm i bootstrap --save
----

* Inclure bootstrap dans le projet angular en modifiant le fichier `angular.json` comme suit :

[source,json]
----
{
  "$schema": "./node_modules/@angular/cli/lib/config/schema.json",
  "version": 1,
  "newProjectRoot": "projects",
  "projects": {
    "zzz": {
      "root": "",
      "sourceRoot": "src",
      "projectType": "application",
      "prefix": "app",
      "schematics": {},
      "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            "outputPath": "dist/zzz",
            "index": "src/index.html",
            "main": "src/main.ts",
            "polyfills": "src/polyfills.ts",
            "tsConfig": "src/tsconfig.app.json",
            "assets": [
              "src/favicon.ico",
              "src/assets"
            ],
            "styles": [
              "src/styles.css",  <---- Ajouter la virgule
              "node_modules/bootstrap/dist/css/bootstrap.css"  <----- Ajouter cette ligne
            ],
            ...
          }
        }
      }
    }
  }
}
----

* Pour les composants dynamiques, vous pouvez intégrer le projet [NG Bootstrap](https://ng-bootstrap.github.io/) et tester quelques composants.


== AvisComponent

* Créer un composant `Avis` qui affiche 2 boutons.

image:images/AvisComponent.png[]

* Vérifier l'affichage des deux boutons dans la page principale.

== Composant `Collegue`

* Créer un fichier `src/app/models.ts` qui va héberger toutes les structures d'encapsulation des données.
* Ajouter y une interface `Collegue` avec les champs suivants : `pseudo`, `score` et `photoUrl`.

* Créer un composant `Collegue` qui affiche la photo, le pseudo et le score d'un collègue et offre la possibilité d'émettre un avis.

image:images/CollegueComponent.png[]

* Ce composant prend en entrée un objet de type `Collegue` (`@Input()`).

* Vérifier l'affichage `CollegueComponent` dans la page principale.
* Créer un objet `Collegue` fictif pour l'affichage.

== Incrémentation

* Ajouter au fichier `src/app/models.ts` une énumération `Avis` avec deux valeurs possibles : `AIMER` et `DETESTER`.

* Modifier le composant `AvisComponent` pour qu'il émette un événement `avis` avec la valeur `Avis.AIMER` ou `Avis.DETESTER` en fonction du bouton cliqué.

* Implémenter une incrémentation / décrémentation du score lors du clic sur un bouton de `AvisComponent`.

== Limitation sur les scores

Mettre en œuvre le comportement suivant :

* Si le score est <= -1000, alors le bouton `Je déteste` se désactive.

* Si le score est >= 1000 alors le bouton `J'aime` se désactive
